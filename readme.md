# EMBL Python User Group

A group of people at EMBL Heidelberg, who share an interest in the Python programming language. You can contact the group by sending email to [python@embl.de](mailto:python@embl.de). The mailing list is managed by [Toby Hodges](mailto:toby.hodges@embl.de), [Malvika Sharan](mailto:malvika.sharan@embl.de), and [Marc Gouw](mailto:gouw@embl.de) - to join us, send an email to one of them. Meetings usually take place fortnightly, on Wednesdays at 17:00, in the Biocomputing Seminar Room (100).

#### Ideas/suggestions for future meetings

Please add to this list, or email one of the mailing list managers above, to suggest/request/volunteer a topic for discussion.

- error handling
- super()
- .py -> .ipynb
- matplotlib 2.0
