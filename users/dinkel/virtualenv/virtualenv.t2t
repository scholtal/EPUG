VirtualEnv
Holger Dinkel, EPUG
Version 1.3, %%date(%d.%m.%Y)

%!include(html): SITEPATH/menu_t2t


+Installation+
Install PIP:
```
wget http://pypi.python.org/pypi/pip/1.1#downloads
tar xzf pip-1.1.tar.gz
cd pip-1.1
python setup.py install
```

Install virtualenv, virtualenvwrapper:
```
pip install virtualenv virtualenvwrapper
```

Show installed packages:
```
pip freeze
```

Use requirements files to install exactly the correct versions:
```
pip freeze > requirements.txt
pip install -r requirements.txt
```



read http://www.doughellmann.com/projects/virtualenvwrapper/

read http://virtualenvwrapper.readthedocs.org/en/latest/index.html

+Usage+
++VirtualenvWrapper++
Features:
+ Organizes all of your virtual environments in one place.
+ Wrappers for managing your virtual environments (create, delete, copy).
+ Use a single command to switch between environments.
+ Tab completion for commands that take a virtual environment as argument.
+ User-configurable hooks for all operations (see Per-User Customization).
+ Plugin system for more creating sharable extensions (see Extending Virtualenvwrapper).


Usage:
create directory to hold your environments:
```
mkdir $WORKON_HOME
```

Then create the Variable WORKON_HOME and source the wrapper script
(it's a good idea to put this into your bashrc):
```
export WORKON_HOME=~/virtualenv
source /usr/local/bin/virtualenvwrapper.sh
```
create a new virtual environment:
```
mkvirtualenv test_environment
```

switch to an existing virtual environment:
```
workon test_envirnoment
```

stop using an environment:
```
deactivate
```

show all environments that exist in folder $WORKON_HOME:
```
lsvirtualenv
```

change into the directory of an environment:
```
cdvirtualenv <env>
```



You can go crazy on customizing the individual scripts for each environment:

in file WORKON_HOME/<ENV>/bin/postactivate
```
export OLD_VIRTUAL_ENV=$PWD
cd $VIRTUAL_ENV/bin
```
in file $WORKON_HOME/<ENV>/bin/postdeactivate
```
cd $OLD_VIRTUAL_ENV
```


%!include(html): SITEPATH/footer_t2t

