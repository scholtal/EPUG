
#. Get a Raspberry Pi
#. Grab the NOOBS (New Out Of the Box Software) image at http://www.raspberrypi.org/downloads/ and put it on a SD card.
#. Boot it & install Raspbian


iPython setup:
--------------

#. Start iPython notebook::

    ipython notebook  --no-browser --port=7777

#. Create a ssh tunnel (pi's IP address here is 10.10.0.100)::

    ssh -N -f -L localhost:7777:localhost:7777 pi@10.10.0.100

#. Browse to the Ipython notebook at http://127.0.0.1:7777/
