# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# Import the GPIO module and set the board layout:

# <codecell>

import random
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

# <markdowncell>

# Initialize the Pins:

# <codecell>

red=27
blue=18
yellow=22
pins = [red,blue,yellow]

GPIO.setup(red, GPIO.OUT)
GPIO.setup(blue, GPIO.OUT)
GPIO.setup(yellow, GPIO.OUT)

def turn_off():
    for pin in pins:
         GPIO.output(pin, False)

turn_off()

# <markdowncell>

# Now switch the LEDs:

# <codecell>

GPIO.output(red, True)
GPIO.output(yellow, False)
GPIO.output(blue, True)

# <codecell>

for i in range(100):
    pin = random.choice(pins)
    mode = random.choice((True, False))
    GPIO.output(pin, mode)
    time.sleep(0.1)
print 'done'
turn_off()

# <codecell>


